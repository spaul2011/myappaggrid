import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { CellcustomComponent } from './cellcustom/cellcustom.component';
import { CustomMatSelectComponent } from './cellcustom/cutom-matselect.componet';

import { 
  MatListModule, MatSelectModule, MatButtonModule, MatCheckboxModule 
} from '@angular/material';

//import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from './angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

@NgModule({
  declarations: [
    AppComponent,
    CellcustomComponent,
    CustomMatSelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatListModule,
    MatButtonModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    AgGridModule.withComponents([CellcustomComponent]),   
    //NgMultiSelectDropDownModule.forRoot()  
    AngularMultiSelectModule
  ],
  providers: [],
  entryComponents:[],
  bootstrap: [AppComponent]
})
export class AppModule { }
