import { Component,OnInit } from '@angular/core';
import { GridOptions } from "ag-grid-community";
import { CellcustomComponent } from './cellcustom/cellcustom.component';

@Component({
  selector: 'dmaa-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'my-ui-app';
  show: boolean = true;
  columnDefs = [
      {headerName: 'Make', field: 'make',checkboxSelection:true,width:100},
      {headerName: 'Model', field: 'model',width:100},
      {headerName: 'Price', field: 'price',width:100},
      {headerName: 'Action',cellRendererFramework:CellcustomComponent,suppressFilter:true,width:300},
  ];
  rowData = [
      { make: 'Toyota', model:"Toyota", price:35000},
      { make: 'Ford', model:"Mondeo", price:32000},
      { make: 'Porsche', model:"Boxter", price:72000}
  ];  

  //cellEditor: 'selectEditor',editable: true
  //cellRendererFramework:CellcustomComponent,suppressFilter:true

  private gridVehicleGroupOptions: GridOptions;
  constructor(){
     this.gridVehicleGroupOptions = <GridOptions>{
        rowData:this.rowData ,
        columnDefs: this.columnDefs,
        rowHeight:50,
        onGridReady: () => {
          //this.gridVehicleGroupOptions.api.sizeColumnsToFit();
        },
        context: {
            componentParent: this,
        },
    }; 
  }
  
  ngOnInit(){        
  } 

  getEmptyRowMessage(){
    return {noRowsToShow: 'VehicleGroup data not available'};
  }
  buttonClick(){
    alert("Button Click invoke");
  }
}