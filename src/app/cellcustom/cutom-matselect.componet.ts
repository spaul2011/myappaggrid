import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgModel } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material';

@Component({
    selector: 'mat-select-check-all',
    template: `
  <div>
  <mat-form-field style="width:100%" class="select-form">
      <mat-select disableOptionCentering [compareWith]="compareWithFn" name="selectionModel"
       [(ngModel)]="model" multiple #selectionModel="ngModel" (ngModelChange)="change($event)">
          <mat-option 
          disabled="disabled" 
          class="filter-option">
            <button 
              mat-raised-button 
              class="mat-primary fill text-sm" 
              (click)="selectAll($event)">
              Select All
            </button>
            <button 
              mat-raised-button 
              class="mat-accent fill text-sm" 
              (click)="deselectAll()">
              Deselect All
            </button>
          </mat-option>
        <mat-option *ngFor="let dropdown of values" [value]="dropdown">{{dropdown.itemName}}</mat-option>
      </mat-select>
    </mat-form-field>
  
</div>
  `,
    styles: ['']
})
export class CustomMatSelectComponent implements OnInit {
    @Input() model = [];
    @Input() values = [];
    @Input() text = 'Select All';
    @Output() onSelectAll = new EventEmitter();
    @Output() onDeSelectAll = new EventEmitter();
    @Output() onItemSelect = new EventEmitter();
    @Output() onItemDeSelect = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    deselectAll() {
        this.model = [];
    }

    selectAll(event:any) {
        this.model = [];
        let clonedValues = this.values.map(x => Object.assign({}, x));
        this.model=clonedValues;
        //this.onSelectAll.emit(this.model);
    }

    compareWithFn(item1, item2) {
        return item1 && item2 ? item1.id === item2.id : item1 === item2;
    }

}
