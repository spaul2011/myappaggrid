import { Component, ViewChild, ViewContainerRef } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-cellcustom',
  templateUrl: './cellcustom.component.html',
  styleUrls: ['./cellcustom.component.scss']
})
export class CellcustomComponent implements ICellRendererAngularComp {

  /*templateUrl: './cellcustom.component.html',
  styleUrls: ['./cellcustom.component.scss']*/

  params: any;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  editModel: boolean = false;

  constructor() { }

  ngOnInit() {

    this.dropdownList = [
      { id: 1, itemName: 'Mumbai' },
      { id: 2, itemName: 'Bangaluru' },
      { id: 3, itemName: 'Pune' },
      { id: 4, itemName: 'Navsari' },
      { id: 5, itemName: 'New Delhi' }
    ];

    this.selectedItems = [{ id: 1, itemName: 'Mumbai' },
    { id: 2, itemName: 'Bangaluru' },
    { id: 3, itemName: 'Pune' }];

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
  }

  agInit(params: any): any {
    this.params = params;
  }

  ngInit() {

  }

  refresh(): boolean {
    return false;
  }

  selectAll(event:any) {
    this.selectedItems=[];
    this.dropdownList.map(o => {
      this.selectedItems.push(o.itemName);
    })
    event=this.selectedItems;
  }

  deselectAll(event:any) {
    this.selectedItems = [];
  }

  onClickEdit(params) {
    console.log(params);
    this.editModel = true;
  }
  onClickSave() {
    this.editModel = false;
  }

  onClickCancel() {
    this.editModel = false;
  }
}

